import pytest

from typing import Generator
from pathlib import Path

from advent.common.data_stream import stream_lines_from_file, parse_line_to_ints, pipe


@pytest.fixture
def fake_file(tmp_path: Path) -> Path:
    data = "\n".join(str(i) for i in range(10))
    file_name = tmp_path / "fake_file.txt"
    file_name.write_text(data)
    return file_name


@pytest.fixture
def fake_empy_file(tmp_path: Path) -> Path:
    file_name = tmp_path / "fake_file.txt"
    file_name.touch()
    return file_name


def test_iter_lines(fake_file: Path):
    data = list(stream_lines_from_file(fake_file))
    assert list(str(i) for i in range(10)) == data


def test_iter_lines_empty_file(fake_empy_file: Path):
    assert [] == list(stream_lines_from_file(fake_empy_file))


def test_parse_line_to_int():
    assert [] == list(parse_line_to_ints(""))
    assert [1] == list(parse_line_to_ints("1"))
    assert [1, 9] == list(parse_line_to_ints("1   9"))
    assert [1, 9, 11, 99, 576] == list(parse_line_to_ints("1   9  \n\t 11 99 576"))


def test_pipe():
    def gen_lines(num_lines: int):
        for i in range(num_lines):
            yield f"{i} \n {i + 10}"

    def split_lines(s: str) -> Generator[str, None, None]:
        for l in s.splitlines():
            yield l

    lines = pipe(gen_lines(3), split_lines)
    data_stream = pipe(lines, parse_line_to_ints)

    assert sorted(data_stream) == [0, 1, 2, 10, 11, 12]
