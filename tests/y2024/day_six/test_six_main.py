from pathlib import Path

import pytest

from advent.y2024.day_six.main import MoveSimulation, exercise_one, exercise_two


@pytest.fixture
def fake_data() -> str:
    data = """
....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#..."""

    return data


@pytest.fixture
def fake_data_file(tmp_path: Path, fake_data: str):
    fake_file = tmp_path / "day_six_fake_data.txt"
    fake_file.write_text(fake_data)
    return fake_file


def test_example_one_fake(fake_data: str):
    lines = fake_data.splitlines()
    sim = MoveSimulation.from_map_str(lines)
    out = sim.simulate()
    assert out == 41


def test_exercise_one_fake(fake_data_file: Path):
    assert exercise_one(fake_data_file) == 41


def test_exercise_one_real():
    assert exercise_one() == 5162


def test_fake_example_two(fake_data: str):
    lines = fake_data.splitlines()
    sim = MoveSimulation.from_map_str(lines)
    out = sim.find_all_loops()
    assert out == 6


def test_exercise_two_fake(fake_data_file: Path):
    assert exercise_two(fake_data_file) == 6


def test_exercise_two_real():
    assert exercise_two() == 1909
