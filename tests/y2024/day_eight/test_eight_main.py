from pathlib import Path

import pytest

from advent.y2024.day_eight.main import (
    Grid,
    exercise_one,
    exercise_two,
)

two_antenae_data = """..........
...#......
..........
....a.....
..........
.....a....
..........
......#...
..........
.........."""


three_antenae_data = """..........
...#......
#.........
....a.....
........a.
.....a....
..#.......
......#...
..........
.........."""


multiple_antenae_data = """..........
...#......
#.........
....a.....
........a.
.....a....
..#.......
......A...
..........
........."""

complicated_antane_data = """......#....#
...#....0...
....#0....#.
..#....0....
....0....#..
.#....A.....
...#........
#......#....
........A...
.........A..
..........#.
..........#."""


@pytest.fixture
def fake_data() -> str:
    return complicated_antane_data


@pytest.fixture
def simple_part_two() -> str:
    return """T....#....
...T......
.T....#...
.........#
..#.......
..........
...#......
..........
....#.....
.........."""


@pytest.fixture
def fake_data_file(tmp_path: Path, fake_data: str):
    nodes_removed = fake_data.replace("#", ".")
    fake_file = tmp_path / "day_eight_fake_data.txt"
    fake_file.write_text(nodes_removed)
    return fake_file


@pytest.mark.parametrize(
    "data",
    (
        two_antenae_data,
        three_antenae_data,
        multiple_antenae_data,
        complicated_antane_data,
    ),
)
def test_simple_antenae(data: str):
    lines = data.splitlines()
    grid = Grid.from_data_lines(lines)
    expected = grid.char_pos_map.pop("#")
    extra_nodes = grid.char_pos_map.get("A", [])
    if len(extra_nodes) >= 1:
        expected.append(extra_nodes[0])
    assert set(expected) == set(grid.iter_valid_nodes())


def test_exercise_one_fake(fake_data_file: Path):
    assert exercise_one(fake_data_file) == 14


def test_exercise_one_real():
    assert exercise_one() == 228


def test_part_two_simple_data(simple_part_two: str):
    lines = simple_part_two.splitlines()
    grid = Grid.from_data_lines(lines)
    expected = grid.char_pos_map.pop("#")
    nodes = set(grid.iter_valid_nodes(extend_range=grid.dim.x, min_range=0))

    assert len(expected) + 3 == len(nodes)


def test_fake_example_two(fake_data: str):
    lines = fake_data.splitlines()
    grid = Grid.from_data_lines(lines)
    grid.char_pos_map.pop("#")
    nodes = set(grid.iter_valid_nodes(extend_range=grid.dim.x, min_range=0))

    assert len(nodes) == 34


def test_exercise_two_fake(fake_data_file: Path):
    assert exercise_two(fake_data_file) == 34


def test_exercise_two_real():
    assert exercise_two() == 766
