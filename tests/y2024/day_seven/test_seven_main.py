from operator import add, mul
from pathlib import Path

import pytest

from advent.common.data_stream import parse_line_to_ints
from advent.y2024.day_seven.main import (
    concat,
    exercise_one,
    exercise_two,
    iter_valid_targets,
)


@pytest.fixture
def fake_data() -> str:
    return (
        "190: 10 19\n"
        "3267: 81 40 27\n"
        "83: 17 5\n"
        "156: 15 6\n"
        "7290: 6 8 6 15\n"
        "161011: 16 10 13\n"
        "192: 17 8 14\n"
        "21037: 9 7 18 13\n"
        "292: 11 6 16 20\n"
    )


@pytest.fixture
def fake_data_file(tmp_path: Path, fake_data: str):
    fake_file = tmp_path / "day_sseven_fake_data.txt"
    fake_file.write_text(fake_data)
    return fake_file


def test_example_one_fake(fake_data: str):
    lines = fake_data.splitlines()
    stream_of_ints = (parse_line_to_ints(l.replace(":", "")) for l in lines)
    total = sum(iter_valid_targets(stream_of_ints, (mul, add)))

    assert total == 3749


def test_exercise_one_fake(fake_data_file: Path):
    assert exercise_one(fake_data_file) == 3749


def test_exercise_one_real():
    assert exercise_one() == 3312271365652


def test_fake_example_two(fake_data: str):
    lines = fake_data.splitlines()
    stream_of_ints = (parse_line_to_ints(l.replace(":", "")) for l in lines)
    total = sum(iter_valid_targets(stream_of_ints, (mul, add, concat)))

    assert total == 11387


def test_exercise_two_fake(fake_data_file: Path):
    assert exercise_two(fake_data_file) == 11387


def test_exercise_two_real():
    assert exercise_two() == 509463489296712
