from pathlib import Path
from typing import Iterator

import pytest

from advent.y2024.day_five.main import UpdateGraph, exercise_one, exercise_two


@pytest.fixture
def fake_data() -> Iterator[str]:
    data = """47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47"""

    return iter(data.splitlines())


@pytest.fixture
def fake_data_file(tmp_path: Path, fake_data: list[str]):
    fake_file = tmp_path / "day_five_fake_data.txt"
    data = "\n".join(l for l in fake_data)
    fake_file.write_text(data)
    return fake_file


def test_example_one_fake(fake_data: list[str]):
    graph = UpdateGraph.from_lines(fake_data)
    assert sum(graph.iter_valid_pages()) == 143


def test_exercise_one_fake(fake_data_file: Path):
    assert exercise_one(fake_data_file) == 143


def test_exercise_one_real():
    assert exercise_one() == 6242


def test_fake_example_two(fake_data: list[str]):
    graph = UpdateGraph.from_lines(fake_data)
    assert sum(graph.iter_invalid_pages()) == 123


def test_exercise_two_fake(fake_data_file: Path):
    assert exercise_two(fake_data_file) == 123


def test_exercise_two_real():
    assert exercise_two() == 5169
