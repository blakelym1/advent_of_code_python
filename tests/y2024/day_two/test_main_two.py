import pytest
from typing import Tuple
from pathlib import Path
from advent.y2024.day_two.main import (
    is_line_valid,
    is_line_valid_dampener,
    exercise_one,
    exercise_two,
)


@pytest.fixture
def data_lines():
    return (
        (7, 6, 4, 2, 1),
        (1, 2, 7, 8, 9),
        (9, 7, 6, 2, 1),
        (1, 3, 2, 4, 5),
        (8, 6, 4, 4, 1),
        (1, 3, 6, 7, 9),
    )


@pytest.fixture
def expected_safe_lines_part_one():
    return [True, False, False, False, False, True]


@pytest.fixture
def expected_safe_lines_part_two():
    return [True, False, False, True, True, True]


@pytest.fixture
def fake_data_file(tmp_path: Path, data_lines: Tuple[Tuple[int, ...], ...]):
    fake_file = tmp_path / "day_two_fake_data.txt"
    data = "\n".join(" ".join(str(d) for d in l) for l in data_lines)
    fake_file.write_text(data)
    return fake_file


def test_is_list_valid(
    data_lines: Tuple[Tuple[int, ...], ...], expected_safe_lines_part_one: list[bool]
):
    for expected, line in zip(expected_safe_lines_part_one, data_lines):
        assert expected == is_line_valid(line)


def test_is_list_valid_dampener(
    data_lines: Tuple[Tuple[int, ...]], expected_safe_lines_part_two: list[bool]
):
    for expected, line in zip(expected_safe_lines_part_two, data_lines):
        assert expected == is_line_valid_dampener(line)


def test_exercise_one(fake_data_file: Path, expected_safe_lines_part_one: list[bool]):
    assert sum(expected_safe_lines_part_one) == exercise_one(fake_data_file)


def test_exercise_two(fake_data_file: Path, expected_safe_lines_part_two: list[bool]):
    assert sum(expected_safe_lines_part_two) == exercise_two(fake_data_file)


def test_exercise_one_real():
    assert exercise_one() == 479


def test_exercise_two_real():
    assert exercise_two() == 531
