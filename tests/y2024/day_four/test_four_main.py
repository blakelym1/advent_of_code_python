from pathlib import Path

import pytest

from advent.common.structures import Trie
from advent.y2024.day_four.main import MessageGrid, exercise_one, exercise_two


@pytest.fixture
def minimal_example() -> list[str]:
    return [
        "..X...",
        ".SAMX.",
        ".A..A.",
        "XMAS.S",
        ".X....",
    ]


@pytest.fixture
def minimal_example_two() -> list[str]:
    return [
        "M.S",
        ".A.",
        "M.S",
    ]


@pytest.fixture
def fake_data_one() -> list[str]:
    return [
        "MMMSXXMASM",
        "MSAMXMSMSA",
        "AMXSXMAAMM",
        "MSAMASMSMX",
        "XMASAMXAMM",
        "XXAMMXXAMA",
        "SMSMSASXSS",
        "SAXAMASAAA",
        "MAMMMXMMMM",
        "MXMXAXMASX",
    ]


@pytest.fixture
def fake_data_minimal() -> list[str]:
    return [
        "....XXMAS.",
        ".SAMXMS...",
        "...S..A...",
        "..A.A.MS.X",
        "XMASAMX.MM",
        "X.....XA.A",
        "S.S.S.S.SS",
        ".A.A.A.A.A",
        "..M.M.M.MM",
        ".X.X.XMASX",
    ]


@pytest.fixture
def fake_data_two() -> list[str]:
    return [
        ".M.S......",
        "..A..MSMS.",
        ".M.S.MAA..",
        "..A.ASMSM.",
        ".M.S.M....",
        "..........",
        "S.S.S.S.S.",
        ".A.A.A.A..",
        "M.M.M.M.M.",
        "..........",
    ]


@pytest.fixture
def fake_data_file_one(tmp_path: Path, fake_data_one: list[str]):
    fake_file = tmp_path / "day_four_fake_data.txt"
    data = "\n".join(l for l in fake_data_one)
    fake_file.write_text(data)
    return fake_file


@pytest.fixture
def fake_data_file_two(tmp_path: Path, fake_data_two: list[str]):
    fake_file = tmp_path / "day_four_fake_data.txt"
    data = "\n".join(l for l in fake_data_two)
    fake_file.write_text(data)
    return fake_file


def test_minimal_example(minimal_example: list[str]):
    messages = Trie()
    messages.insert("XMAS")
    grid = MessageGrid.from_lines(minimal_example, messages, "X")
    assert len(list(grid.iter_origins())) == 4
    assert sum(grid.count_messages(pos) for pos in grid.iter_origins()) == 4


def test_example_one_minimal(fake_data_minimal: list[str]):
    messages = Trie()
    messages.insert("XMAS")
    grid = MessageGrid.from_lines(fake_data_minimal, messages, "X")
    total = grid.count_all_messages(grid.count_messages)
    assert total == 18


def test_example_one_fake(fake_data_one: list[str]):
    messages = Trie()
    messages.insert("XMAS")
    grid = MessageGrid.from_lines(fake_data_one, messages, "X")
    total = grid.count_all_messages(grid.count_messages)
    assert total == 18


def test_exercise_one_fake(fake_data_file_one: Path):
    assert exercise_one(fake_data_file_one) == 18


def test_exercise_one_real():
    assert exercise_one() == 2569


def test_minimal_example_two(minimal_example_two: list[str]):
    messages = Trie()
    messages.insert("MAS")
    messages.insert("SAM")
    grid = MessageGrid.from_lines(minimal_example_two, messages, "A")
    assert len(list(grid.iter_origins())) == 1
    assert sum(grid.count_x_messages(pos) for pos in grid.iter_origins()) == 1


def test_fake_example_two(fake_data_two: list[str]):
    messages = Trie()
    messages.insert("MAS")
    messages.insert("SAM")
    grid = MessageGrid.from_lines(fake_data_two, messages, "A")
    assert len(list(grid.iter_origins())) == 9
    assert sum(grid.count_x_messages(pos) for pos in grid.iter_origins()) == 9


def test_exercise_two_fake(fake_data_file_one: Path):
    assert exercise_two(fake_data_file_one) == 9


def test_exercise_two_real():
    assert exercise_two() == 1998
