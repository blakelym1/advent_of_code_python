from pathlib import Path

import pytest

from advent.y2024.day_three.main import (
    exercise_one,
    exercise_two,
    parse_mult_op_to_ints,
    parse_mult_op_to_ints_conditional,
)


@pytest.fixture
def fake_data_one() -> str:
    return "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"


@pytest.fixture
def fake_data_two() -> str:
    return "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"


@pytest.fixture
def expected_safe_lines_part_one():
    return [True, False, False, False, False, True]


@pytest.fixture
def expected_safe_lines_part_two():
    return [True, False, False, True, True, True]


@pytest.fixture
def fake_data_file_one(tmp_path: Path, fake_data_one: str):
    fake_file = tmp_path / "day_three_fake_data.txt"
    fake_file.write_text(fake_data_one)
    return fake_file


def test_example_data_mult(fake_data_one: str):
    # 161 (2*4 + 5*5 + 11*8 + 8*5).
    out = sum(g[0] * g[1] for g in parse_mult_op_to_ints(fake_data_one))
    assert out == 161


def test_example_data_mult_cond(fake_data_two: str):
    # 161 (2*4 + 5*5 + 11*8 + 8*5).
    out = sum(g[0] * g[1] for g in parse_mult_op_to_ints_conditional(fake_data_two))
    assert out == 48


def test_exercise_one_fake(fake_data_file_one: Path):
    assert exercise_one(fake_data_file_one) == 161


def test_exercise_one_real():
    assert exercise_one() == 184511516


def test_exercise_two_real():
    assert exercise_two() == 90044227
