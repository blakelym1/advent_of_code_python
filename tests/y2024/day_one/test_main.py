import pytest
from collections import Counter
from advent.y2024.day_one.main import (
    compute_distance,
    exercise_one,
    exercise_two,
    compute_similarity,
)
from pathlib import Path


@pytest.fixture
def example_data() -> tuple[list[int], list[int]]:
    data = [
        (3, 4),
        (4, 3),
        (2, 5),
        (1, 3),
        (3, 9),
        (3, 3),
    ]
    return tuple(list(d) for d in zip(*data))  # type: ignore


@pytest.fixture
def example_data_file(
    example_data: tuple[list[int], list[int]], tmp_path: Path
) -> Path:
    data = "\n".join(f"{d[0]}   {d[1]}" for d in zip(example_data[0], example_data[1]))
    fake_path = tmp_path / "fake.txt"
    fake_path.write_text(data)
    return fake_path


def test_compute_distance(example_data: tuple[list[int], list[int]]):
    for d in example_data:
        d.sort()
    out = compute_distance(*example_data)

    assert out == 11


def test_compute_similarity(example_data: tuple[list[int], list[int]]):
    counters = tuple(Counter(l) for l in example_data)
    assert compute_similarity(*counters) == 31


def test_exercise_one(example_data_file: Path):
    assert exercise_one(example_data_file) == 11


def test_exercise_two(example_data_file: Path):
    assert exercise_two(example_data_file) == 31
