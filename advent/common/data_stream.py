from pathlib import Path
from typing import Callable, Generator, Iterable, Iterator, TypeVar

T = TypeVar("T")
U = TypeVar("U")


def stream_lines_from_file(f_path: Path) -> Generator[str, None, None]:
    with f_path.open() as f:
        for line in f:
            yield line.rstrip()


def parse_line_to_ints(line: str) -> Generator[int, None, None]:
    for i in line.split():
        yield int(i)


def parse_lines_to_coordinates(
    data_lines: Iterable[str],
) -> Iterator[tuple[tuple[int, int], str]]:
    for i, line in enumerate(data_lines):
        for j, char in enumerate(line):
            yield (i, j), char


def pipe(
    in_stream: Generator[T, None, None],
    out_stream_gen: Callable[[T], Generator[U, None, None]],
) -> Generator[U, None, None]:
    for data in in_stream:
        yield from out_stream_gen(data)
