from collections import defaultdict
from dataclasses import dataclass, field
from enum import Enum


class TrieStatus(Enum):
    NOT_IN = -1
    PREFIX = 0
    WORD = 1


@dataclass
class TrieNode:
    children: defaultdict[str, "TrieNode"] = field(
        default_factory=lambda: defaultdict(TrieNode)
    )
    is_end: bool = False


@dataclass
class Trie:
    root: TrieNode = field(default_factory=TrieNode)
    max_depth: int = 0

    def insert(self, word: str) -> None:
        current = self.root
        for letter in word:
            current = current.children[letter]
        current.is_end = True
        if len(word) > self.max_depth:
            self.max_depth = len(word)

    def search(self, word: str) -> bool:
        current = self.root
        for letter in word:
            current = current.children.get(letter)
            if current is None:
                return False
        return current.is_end

    def presence(self, prefix: str) -> TrieStatus:
        current = self.root

        for letter in prefix:
            current = current.children.get(letter)
            if not current:
                return TrieStatus.NOT_IN

        if current.is_end:
            return TrieStatus.WORD

        return TrieStatus.PREFIX
