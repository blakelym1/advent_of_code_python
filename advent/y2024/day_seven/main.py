from itertools import product
from operator import add, mul
from pathlib import Path
from typing import Callable, Iterable, Iterator

from advent.common.data_stream import parse_line_to_ints, stream_lines_from_file

DATA_FILE = Path(__file__).parent / "data_seven.txt"


def concat(a: int, b: int) -> int:
    return int(str(a) + str(b))


def iter_valid_targets(
    stream_of_ints: Iterable[Iterable[int]],
    operators: Iterable[Callable[[int, int], int]],
) -> Iterator[int]:
    for target, *operands in stream_of_ints:
        for select_ops in product(operators, repeat=len(operands) - 1):
            # add mult
            acum = select_ops[0](*operands[:2])
            for val, op in zip(operands[2:], select_ops[1:]):
                acum = op(acum, val)
            if target == acum:
                yield target
                break


def exercise_one(data_file: Path = DATA_FILE) -> int:
    data = stream_lines_from_file(data_file)
    stream_of_ints = (parse_line_to_ints(l.replace(":", "")) for l in data)
    return sum(iter_valid_targets(stream_of_ints, (mul, add)))


def exercise_two(data_file: Path = DATA_FILE) -> int:
    data = stream_lines_from_file(data_file)
    stream_of_ints = (parse_line_to_ints(l.replace(":", "")) for l in data)
    return sum(iter_valid_targets(stream_of_ints, (mul, add, concat)))


if __name__ == "__main__":
    from time import time

    start = time()
    print(f"exercise one:{exercise_one()} duration: {time() - start}")
    start = time()
    print(f"exercise two:{exercise_two()}")
    print(f"total duration: {time() - start}")
