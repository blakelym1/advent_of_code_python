from collections import defaultdict
from dataclasses import dataclass
from itertools import permutations
from pathlib import Path
from typing import Any, Iterable, Iterator

from advent.common.data_stream import parse_lines_to_coordinates, stream_lines_from_file

DATA_FILE = Path(__file__).parent / "data_eight.txt"


@dataclass(frozen=True)
class Pos:
    y: int
    x: int

    def __mul__(self, other: int) -> "Pos":
        return Pos(self.y * other, self.x * other)

    def __sub__(self, other: "Pos") -> "Pos":
        return Pos(self.y - other.y, self.x - other.x)

    def __add__(self, other: "Pos") -> "Pos":
        return Pos(self.y + other.y, self.x + other.x)

    def __lt__(self, other: "Pos") -> bool:
        return self.y < other.y or self.x < other.x

    def __gt__(self, other: "Pos") -> bool:
        return self.y > other.y or self.x > other.x

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, Pos):
            return False
        return self.y == other.y and self.x == other.x


@dataclass
class Grid:
    dim: Pos
    char_pos_map: dict[str, list[Pos]]

    @classmethod
    def from_data_lines(cls, lines: Iterable[str]) -> "Grid":
        char_pos_map: dict[str, list[Pos]] = defaultdict(list)
        pos = Pos(0, 0)
        for coor, char in parse_lines_to_coordinates(lines):
            pos = Pos(*coor)
            if char == ".":
                continue

            char_pos_map[char].append(pos)

        return cls(pos, char_pos_map)

    def iter_all_perm_antenae_pairs(self) -> Iterator[tuple[Pos, Pos]]:
        for pos_list in self.char_pos_map.values():
            if len(pos_list) < 2:
                continue
            yield from permutations(pos_list, 2)

    def iter_valid_nodes(
        self, extend_range: int = 1, min_range: int = 1
    ) -> Iterator[Pos]:
        """search one direction for all permutation of pairs.

        default uses max range of 1 past the originating antenae
        and excludes the originating antenae.

        """
        origin = Pos(0, 0)
        for pos_a, pos_b in self.iter_all_perm_antenae_pairs():
            # find th direction to search in
            direction_vec = pos_a - pos_b

            # extend the direction vector to the limit
            for i in range(min_range, extend_range + 1, 1):
                node = pos_a + (direction_vec * i)

                # if the node is outside of the grid, stop
                if node > self.dim or node < origin:
                    break

                yield node


def exercise_one(data_file: Path = DATA_FILE) -> int:
    data = stream_lines_from_file(data_file)
    grid = Grid.from_data_lines(data)
    return len(set(grid.iter_valid_nodes()))


def exercise_two(data_file: Path = DATA_FILE) -> int:
    data = stream_lines_from_file(data_file)
    grid = Grid.from_data_lines(data)
    return len(set(grid.iter_valid_nodes(extend_range=grid.dim.x, min_range=0)))


if __name__ == "__main__":
    from time import time

    start = time()
    print(f"exercise one:{exercise_one()}")
    print(f"total duration: {time() - start}")
    start = time()
    print(f"exercise two:{exercise_two()}")
    print(f"total duration: {time() - start}")
