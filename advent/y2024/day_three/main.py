import re
from pathlib import Path
from typing import Callable, Generator

DATA_FILE = Path(__file__).parent / "data_three.txt"


def parse_mult_op_to_ints(data: str) -> Generator[tuple[int, int], None, None]:
    for match in re.finditer(r"mul\((\d{1,3}),(\d{1,3})\)", data):
        g = match.groups()
        yield (int(g[0]), int(g[1]))


def parse_mult_op_to_ints_conditional(
    data: str,
) -> Generator[tuple[int, int], None, None]:
    pattern = r"(mul)\((\d{1,3}),(\d{1,3})\)|(do)\(\)|(don't)\(\)"
    # iterate through the data, parsing w. the regex pattern
    # the first and last two group indexes are mutually exclusive
    mult_enabled = True
    for match in re.finditer(pattern, data):
        match match.groups():
            case (str(), str(arg0), str(arg1), None, None):
                if mult_enabled:
                    yield (int(arg0), int(arg1))
            case (None, _, _, str(_), None):
                mult_enabled = True
            case (None, _, _, None, str(_)):
                mult_enabled = False


# TODO:
def sum_mult_from_parser(
    parser: Callable[[str], Generator[tuple[int, int], None, None]],
    data_file: Path = DATA_FILE,
):
    data_stream = parser(data_file.read_text())

    return sum(op[0] * op[1] for op in data_stream)


def exercise_one(data_file: Path = DATA_FILE) -> int:
    return sum_mult_from_parser(parse_mult_op_to_ints, data_file)


def exercise_two(data_file: Path = DATA_FILE) -> int:
    return sum_mult_from_parser(parse_mult_op_to_ints_conditional, data_file)


if __name__ == "__main__":
    print(f"mult operation: {exercise_one()}")
    print(f"conditional mult operations: {exercise_two()}")
