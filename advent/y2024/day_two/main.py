from collections import Counter
from itertools import combinations
from pathlib import Path

from advent.common.data_stream import parse_line_to_ints, stream_lines_from_file

DATA_FILE = Path(__file__).parent / "data_two.txt"


def test(b: int ) -> int:
    a = [1, 2, 3, 4, 5]
    return b + "str"

def line_to_diff_counter(ints: tuple[int, ...]) -> Counter[int]:
    dist_stream = (i - j for i, j in zip(ints, ints[1:]))
    return Counter(dist_stream)


def normalize_dist_total(counts: Counter[int]) -> int:
    valid_dist = tuple(range(1, 4))
    pos_total = sum(counts.get(i, 0) for i in valid_dist)
    neg_total = sum(counts.get(-i, 0) for i in valid_dist)
    return max(pos_total, neg_total)


def is_line_valid(ints: tuple[int, ...]) -> bool:
    dist_counts = line_to_diff_counter(ints)
    diff_dist_total = normalize_dist_total(dist_counts)

    return diff_dist_total == dist_counts.total()


def cond_print(should_print: bool, msg: str):
    if not should_print:
        return
    print(msg)


def is_subline_valid(ints: tuple[int, ...]) -> bool:
    subline_valid = False
    for subline in combinations(ints, len(ints) - 1):
        subline_valid = is_line_valid(subline)
        if subline_valid:
            break
    return subline_valid


def is_line_valid_dampener(ints: tuple[int, ...]) -> bool:
    dist_counts = line_to_diff_counter(ints)
    diff_dist_total = normalize_dist_total(dist_counts)
    dist_total = dist_counts.total()

    # optimization, if there are 2 or less inconsistent
    # distance, removing one might fix
    if diff_dist_total == dist_total:
        return True
    elif not (dist_total - diff_dist_total <= 2):
        return False

    return is_subline_valid(ints)


def exercise_one(data_file: Path | None = None):
    line_stream = stream_lines_from_file(data_file or DATA_FILE)
    return sum(is_line_valid(tuple(parse_line_to_ints(l))) for l in line_stream)


def exercise_two(data_file: Path | None = None):
    line_stream = stream_lines_from_file(data_file or DATA_FILE)
    return sum(
        is_line_valid_dampener(tuple(parse_line_to_ints(l))) for l in line_stream
    )


if __name__ == "__main__":
    print(f"{exercise_one()=}")
    print(f"{exercise_two()=}")
