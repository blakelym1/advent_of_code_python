from dataclasses import dataclass, field
from itertools import groupby
from pathlib import Path
from typing import Iterable

from advent.common.data_stream import stream_lines_from_file

DATA_FILE = Path(__file__).parent / "data_six.txt"


@dataclass(slots=True)
class DirNode:
    name: str
    vector: tuple[int, int]
    turn: "DirNode"


def make_dir_chain() -> DirNode:
    up = DirNode("UP", (-1, 0), None)  # type: ignore
    left = DirNode("LEFT", (0, -1), up)
    down = DirNode("DOWN", (1, 0), left)
    right = DirNode("RIGHT", (0, 1), down)
    up.turn = right

    return up


@dataclass(slots=True)
class Guard:
    pos: tuple[int, int]
    dir: DirNode = field(default_factory=make_dir_chain)

    def get_next_step(self) -> tuple[int, int]:
        return (self.pos[0] + self.dir.vector[0], self.pos[1] + self.dir.vector[1])

    def turn(self) -> None:
        self.dir = self.dir.turn

    def move(self) -> tuple[int, int]:
        old_pos = self.pos
        self.pos = self.get_next_step()
        return old_pos

    def move_event(self) -> tuple[tuple[int, int], str]:
        return (self.pos, self.dir.name)


@dataclass
class MoveSimulation:
    mapped_area: dict[tuple[int, int], str]
    starting_pos: tuple[int, int]
    obstacles: set[str] = field(default_factory=lambda: set(("#", "O")))
    count: int = 0
    is_last_move: bool = False
    history: set[tuple[tuple[int, int], str]] = field(default_factory=set)

    def duplicate(self) -> "MoveSimulation":
        d = {k: v for k, v in self.mapped_area.items()}
        obs = {s for s in self.obstacles}
        his = {e for e in self.history}
        start = self.starting_pos
        return MoveSimulation(d, start, obs, history=his)

    @classmethod
    def from_map_str(cls, data_lines: Iterable[str]) -> "MoveSimulation":
        d: dict[tuple[int, int], str] = {}
        current = (0, 0)
        for i, line in enumerate(data_lines):
            for j, char in enumerate(line):
                if char == "^":
                    current = (i, j)
                d[(i, j)] = char

        return cls(d, current)

    def turn_until_can_move(self, guard: Guard) -> bool:
        is_last_move = False
        can_move = False

        while can_move is False:
            next_step = guard.get_next_step()
            next_object = self.mapped_area.get(next_step)

            if next_object is None:
                is_last_move = True
                can_move = True
            elif next_object in self.obstacles:
                guard.turn()
            else:
                can_move = True

        return is_last_move

    def move(self, guard: Guard) -> tuple[int, int]:
        self.is_last_move = self.turn_until_can_move(guard)
        last_pos = guard.move()

        return last_pos

    def get_map_str(self) -> str:
        lines = groupby(self.mapped_area.items(), key=lambda x: x[0][0])
        return "\n".join("".join(d[1] for d in l[1]) for l in lines)

    def simulate(self) -> int:
        guard = Guard(self.starting_pos)
        while not self.is_last_move:
            last_pos = self.move(guard)
            if self.mapped_area[last_pos] != "X":
                self.count += 1
                self.mapped_area[last_pos] = "X"

        return self.count

    def detect_loop(self) -> bool:
        guard = Guard(self.starting_pos)
        loop_found = False
        while not self.is_last_move and not loop_found:
            self.move(guard)
            event = guard.move_event()
            if event in self.history:
                loop_found = True
            self.history.add(event)

        return loop_found

    def find_all_loops(self) -> int:
        guard = Guard(self.starting_pos)
        loop_obstacles: set[tuple[int, int]] = {self.starting_pos}
        loop_count = 0
        while not self.is_last_move:
            self.move(guard)
            if not self.is_last_move and guard.pos not in loop_obstacles:
                loop_sim = self.duplicate()
                loop_sim.mapped_area[guard.pos] = "O"
                loop_obstacles.add(guard.pos)
                if loop_sim.detect_loop():
                    loop_count += 1

        return loop_count


def exercise_one(data_file: Path = DATA_FILE) -> int:
    data = stream_lines_from_file(data_file)
    sim = MoveSimulation.from_map_str(data)
    return sim.simulate()


def exercise_two(data_file: Path = DATA_FILE) -> int:
    data = stream_lines_from_file(data_file)
    sim = MoveSimulation.from_map_str(data)
    return sim.find_all_loops()


if __name__ == "__main__":
    # print(f"exercise one:{exercise_one()}")
    from time import time

    start = time()
    print(f"exercise two:{exercise_two()}")
    print(f"total duration: {time() - start}")
