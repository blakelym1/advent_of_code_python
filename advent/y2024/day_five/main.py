from collections import defaultdict
from dataclasses import dataclass
from functools import cmp_to_key
from itertools import takewhile
from pathlib import Path
from typing import Iterable, Iterator

from advent.common.data_stream import stream_lines_from_file

DATA_FILE = Path(__file__).parent / "data_five.txt"


@dataclass
class UpdateGraph:
    constraints: dict[int, list[int]]
    raw_updates: Iterable[str]

    @classmethod
    def from_lines(cls, data_lines: Iterable[str]) -> "UpdateGraph":
        constraints = defaultdict(list)
        # the first element that doesn't match the predicate is dropped
        # the data file has a blank line between rules and updates
        constraints_lines = takewhile(lambda s: "|" in s, data_lines)
        for line in constraints_lines:
            src, trgt = [int(d) for d in line.split("|")]
            constraints[src].append(trgt)

        return cls(constraints, data_lines)

    def _iter_updates(self) -> Iterator[tuple[int, ...]]:
        for update in self.raw_updates:
            yield tuple(int(u) for u in update.split((",")))

    def _iter_updates_filtered(
        self, return_valid: bool = True
    ) -> Iterator[tuple[int, ...]]:
        for update in self._iter_updates():
            valid = True
            for element, next_element in zip(update, update[1:]):
                if next_element not in self.constraints.get(element, ()):
                    valid = False
                    break
            if valid == return_valid:
                yield update

    def iter_valid_pages(self) -> Iterator[int]:
        for updates in self._iter_updates_filtered():
            num_pages = len(updates)
            if num_pages == 0:
                continue
            yield updates[num_pages // 2]

    def invalid_page_sort(self, this: int, other: int) -> int:
        if this == other:
            return 0
        elif other in self.constraints[this]:
            return -1
        return 1

    def iter_invalid_pages(self) -> Iterator[int]:
        for updates in self._iter_updates_filtered(False):
            num_pages = len(updates)
            if num_pages == 0:
                continue
            sorted_updates = sorted(
                updates,
                key=cmp_to_key(self.invalid_page_sort),
            )
            # sorted_updates = self.topological_sort(updates)
            yield sorted_updates[num_pages // 2]


def exercise_one(data_file: Path = DATA_FILE) -> int:
    update_graph = UpdateGraph.from_lines(stream_lines_from_file(data_file))
    return sum(update_graph.iter_valid_pages())


def exercise_two(data_file: Path = DATA_FILE) -> int:
    update_graph = UpdateGraph.from_lines(stream_lines_from_file(data_file))
    return sum(update_graph.iter_invalid_pages())


if __name__ == "__main__":
    print(f"exercise one:{exercise_one()}")
    print(f"exercise two:{exercise_two()}")
