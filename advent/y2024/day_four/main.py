from collections import defaultdict, namedtuple
from dataclasses import dataclass
from pathlib import Path
from typing import (
    Callable,
    Generator,
    Iterable,
)

from advent.common.data_stream import stream_lines_from_file
from advent.common.structures import Trie, TrieStatus

DATA_FILE = Path(__file__).parent / "data_four.txt"

Pos = namedtuple("Pos", ["r", "c"])


@dataclass
class MessageGrid:
    grid: dict[Pos, str]
    messages: Trie
    origin: str

    @classmethod
    def from_lines(
        cls, lines: Iterable[str], messages: Trie, origin: str
    ) -> "MessageGrid":
        d = defaultdict(lambda: ".")
        for i, line in enumerate(lines):
            for j, char in enumerate(line):
                d[Pos(i, j)] = char

        return cls(d, messages, origin)

    def count_all_messages(self, count_fn: Callable[[Pos], int]) -> int:
        return sum(count_fn(pos) for pos in self.iter_origins())

    def iter_origins(self) -> Generator[Pos, None, None]:
        for pos, char in self.grid.items():
            if self.origin == char:
                yield pos

    def count_messages(self, orig: Pos) -> int:
        count = 0
        all_steps = [
            (1, 0),
            (1, 1),
            (1, -1),
            (0, 1),
            (0, -1),
            (-1, 0),
            (-1, -1),
            (-1, 1),
        ]
        for step in all_steps:
            back_track = ""
            for i in range(self.messages.max_depth):
                pos = Pos(orig[0] + step[0] * i, orig[1] + step[1] * i)
                back_track += self.grid.get(pos, ".")
                match self.messages.presence(back_track):
                    case TrieStatus.WORD:
                        count += 1
                    case TrieStatus.NOT_IN:
                        ...
                    case TrieStatus.PREFIX:
                        ...
        return count

    def count_x_messages(self, orig: Pos) -> int:
        shift_sw, shift_nw = (-1, -1), (-1, 1)
        count = {shift_sw: 0, shift_nw: 0}
        searches = [(shift_sw, (1, 1)), (shift_nw, (1, -1))]
        for shift, step in searches:
            shifted_orig = Pos(orig.r + shift[0], orig.c + shift[1])
            back_track = ""
            for i in range(self.messages.max_depth):
                pos = Pos(shifted_orig[0] + step[0] * i, shifted_orig[1] + step[1] * i)
                back_track += self.grid.get(pos, ".")
                match self.messages.presence(back_track):
                    case TrieStatus.WORD:
                        count[shift] += 1
                    case TrieStatus.NOT_IN:
                        ...
                    case TrieStatus.PREFIX:
                        ...
        return int(all(c > 0 for c in count.values()))


def exercise_one(data_file: Path = DATA_FILE) -> int:
    data_stream = stream_lines_from_file(data_file)
    messages = Trie()
    messages.insert("XMAS")
    grid = MessageGrid.from_lines(data_stream, messages, "X")
    return grid.count_all_messages(grid.count_messages)


def exercise_two(data_file: Path = DATA_FILE) -> int:
    data_stream = stream_lines_from_file(data_file)
    messages = Trie()
    messages.insert("MAS")
    messages.insert("SAM")
    grid = MessageGrid.from_lines(data_stream, messages, "A")
    return grid.count_all_messages(grid.count_x_messages)


if __name__ == "__main__":
    print(f"xmas count: {exercise_one()}")
    print(f"x-mas count: {exercise_two()}")
