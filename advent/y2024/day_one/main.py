from collections import Counter
from itertools import batched
from pathlib import Path

from advent.common.data_stream import parse_line_to_ints, pipe, stream_lines_from_file

DATA_FILE = Path(__file__).parent / "data_one.txt"


def compute_distance(loc_list1: list[int], loc_list2: list[int]) -> int:
    """Return the summed distance between elements.

    This assumes both lists are sorted

    iterate through each pair of elements and find the distaance between
    aka. the absolute value of the two values subtracted. Sum the differance.

    Args:
        loc_list1 (list[int]): sorted location ints
        loc_list2 (list[int]): sorted location ints

    Returns:
        int: pair wise sum of elements
    """

    return sum(abs(loc[0] - loc[1]) for loc in zip(loc_list1, loc_list2))


def compute_similarity(loc_counter_1: Counter[int], loc_counter_2: Counter[int]) -> int:
    """Calculate simularity based off of the frequency of an int in each list

    frequency_in_list2 * value * frequency_in_list_1

    Args:
        loc_counter_1 (Counter[int]): Counter of ints and how often they appear
        loc_counter_2 (Counter[int]): Counter of ints and how often they appear

    Returns:
        int: similarity
    """
    return sum(
        loc_counter_2[key] * key * loc_counter_1[key] for key in loc_counter_1.keys()
    )


def exercise_one(data_file: Path | None = None) -> int:
    # create a stream of integers from the data file
    data_stream = pipe(
        stream_lines_from_file(data_file or DATA_FILE), parse_line_to_ints
    )

    # pull data stream, batched into 2 ints, split stream with zip and sort
    loc_lists = tuple(sorted(x) for x in zip(*batched(data_stream, 2)))

    return compute_distance(*loc_lists)


def exercise_two(data_file: Path | None = None) -> int:
    # create a stream of integers from the data file
    data_stream = pipe(
        stream_lines_from_file(data_file or DATA_FILE), parse_line_to_ints
    )

    # stream through ints batched into 2 pivoted with zip
    loc_counters: list[Counter[int]] = list(
        Counter(x) for x in zip(*batched(data_stream, 2))
    )

    return compute_similarity(*loc_counters)


if __name__ == "__main__":
    print(f"Total distance: {exercise_one()}")
    print(f"Total similarity: {exercise_two()}")
